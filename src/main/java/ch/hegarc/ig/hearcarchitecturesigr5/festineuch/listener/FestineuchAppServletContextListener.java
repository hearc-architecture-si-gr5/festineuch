/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hegarc.ig.hearcarchitecturesigr5.festineuch.listener;

import ch.hearcarchitecturesigr5.festineuch.persistence.MockData;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.ws.rs.Path;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author mael.voyame
 */
@WebListener
public class FestineuchAppServletContextListener implements ServletContextListener {
    
    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        
        try {
            List<Concert> concerts_temp = new ArrayList();
            List<Artiste> artistes_temp = new ArrayList();
            List<Scene> scenes_temp = new ArrayList();
            List<Evenement> events_temp = new ArrayList();
            Evenement event_temp = new Evenement();
            Concert concert_temp = new Concert();
            Artiste artiste_temp = new Artiste();
            Scene scene_temp = new Scene();
            
            //Si 1 alors c'est une nouvelle scène
            int checkNewScene = 1;
            
            //Pour le premier concert
            int checkConcert = 0;
            
            Date date_temp;
            String string_date_temp;
            
            DocumentBuilder parser = factory.newDocumentBuilder();
            
            Class myclass = FestineuchAppServletContextListener.class;
            URL url = myclass.getResource("FestineuchAppServletContextListener.class");
            String allPath = url.toString().substring(0,url.toString().indexOf("target"));
            String goodPath = allPath.substring(allPath.indexOf(":/")+2);
            String replace = goodPath.replace("/", "\\");
                    
            File xmlFile = new File(replace+"\\XML\\prog19.xml");
            
            Document doc = parser.parse(xmlFile);
            
            //Normalize the XML Structure; It's just too important !!
            doc.getDocumentElement().normalize();
            
            //Here comes the root node
            Element root = doc.getDocumentElement();

            //Se postitionne sur le premier noeud concert
            NodeList nListConcert = doc.getElementsByTagName("concert");
            NodeList nListArtiste;
            NodeList nListScene;
 
            for (int temp = 0; temp < nListConcert.getLength(); temp++)
            {
                Node nodeConcert = nListConcert.item(temp);
                
                concert_temp = new Concert();

                //Create all concerts
                if (nodeConcert.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElementConcert = (Element) nodeConcert;

                    //-------------Set Concert
                    //ID
                    concert_temp.setId(eElementConcert.getAttribute("id"));
                    
                    //Date
                    string_date_temp = eElementConcert.getElementsByTagName("date").item(0).getTextContent();
                    date_temp = new SimpleDateFormat("yyyy-MM-dd").parse(string_date_temp);
                    concert_temp.setDate(date_temp);
                    
                    //Heure début
                    string_date_temp = eElementConcert.getElementsByTagName("debut").item(0).getTextContent();
                    date_temp = new SimpleDateFormat("hh:mm").parse(string_date_temp); 
                    concert_temp.setHeure_debut(date_temp);
                    
                    //Heure fin
                    string_date_temp = eElementConcert.getElementsByTagName("fin").item(0).getTextContent();
                    date_temp = new SimpleDateFormat("hh:mm").parse(string_date_temp); 
                    concert_temp.setHeure_fin(date_temp);
                    
                    
                    if(checkConcert == 0){
                        //Création d'événements pour le concert 101
                        event_temp = new Evenement();

                        event_temp.setId("401");
                        event_temp.setHeure("16:00");
                        event_temp.setRemarque("Retard pour le concert de Patti Smith");

                        events_temp.add(event_temp);


                        event_temp = new Evenement();

                        event_temp.setId("402");
                        event_temp.setHeure("16:15");
                        event_temp.setRemarque("L'artiste Patti Smith se prépare. Elle sera bientôt là");

                        events_temp.add(event_temp);


                        event_temp = new Evenement();

                        event_temp.setId("403");
                        event_temp.setHeure("16:20");
                        event_temp.setRemarque("Le concert va commencer !!!");

                        events_temp.add(event_temp);

                        MockData.setEvenements(events_temp);
                        
                        concert_temp.setRemarques(events_temp);
                        
                        checkConcert = 1;
                    }

                    
                    //-------------Set Artiste
                    nListArtiste = doc.getElementsByTagName("artiste");
                    Node nodeArtiste = nListArtiste.item(temp);
                    
                    if (nodeArtiste.getNodeType() == Node.ELEMENT_NODE){
                        Element eElementArtiste = (Element) nodeArtiste;
                        
                        artiste_temp = new Artiste();
                        
                        //ID
                        artiste_temp.setId(eElementArtiste.getAttribute("id"));
                        
                        //Style
                        artiste_temp.setGenre(eElementArtiste.getAttribute("style"));
                    
                        //Origine
                        artiste_temp.setOrigine(eElementArtiste.getAttribute("origine"));
                        
                        //Nom
                        artiste_temp.setNom(eElementArtiste.getElementsByTagName("nom").item(0).getTextContent());
                    
                        //Description
                        artiste_temp.setDescription(eElementArtiste.getElementsByTagName("description").item(0).getTextContent());
                        
                        //Ajouter artiste à la liste
                        artistes_temp.add(artiste_temp);
                        
                        //Ajouter artiste au concert
                        concert_temp.setArtiste(artiste_temp);
                    }
                    
                    
                    //-------------Set Scene
                    nListScene = doc.getElementsByTagName("scene");
                    Node nodeScene = nListScene.item(temp);
                    
                    if (nodeScene.getNodeType() == Node.ELEMENT_NODE){
                        Element eElementScene = (Element) nodeScene;
                        
                        scene_temp = new Scene();
                        
                        //ID
                        scene_temp.setId(eElementScene.getAttribute("id"));
                        
                        //Nom
                        scene_temp.setNom(eElementScene.getElementsByTagName("nom").item(0).getTextContent());
                        
                        //Description
                        //scene_temp.setDescription(eElementScene.getElementsByTagName("description").item(0).getTextContent());

                        //Au premier passage (concert), la liste est vide donc on y ajoute la scene
                        if(scenes_temp.isEmpty()){
                            //Ajouter la scène dans la liste
                            scenes_temp.add(scene_temp);
                            //Ajoute la scene au concert
                            concert_temp.setScene(scene_temp);
                        }      
                        
                        Scene scene_recup = new Scene();
                        
                        //Initialisation du check - Pour l'instant 
                        checkNewScene = 1;
                        
                        //Vérification si la scène n'est pas déjà dans la liste
                        for(int i=0; i<scenes_temp.size(); i++){
                            if(scenes_temp.get(i).equals(scene_temp)){
                                scene_recup = new Scene();
                                
                                scene_recup = scenes_temp.get(i);

                                //Ajoute la scene au concert
                                concert_temp.setScene(scene_recup);
                                
                                //Si on passe ici, ce n'est pas un nouvelle scène à ajouter
                                checkNewScene = 0;
                            }
                        }
                        
                        //Si checkNewScene est à 1, alors on ajoute une nouvelle scène
                        if(checkNewScene == 1){
                            //Ajouter la scène dans la liste
                            scenes_temp.add(scene_temp);
                            //Ajoute la scene au concert
                            concert_temp.setScene(scene_temp);
                        }
                    }

                    //LOG - Affichage du concert
                    System.out.println(concert_temp.toString());
                }        
                
                //Ajouter le concert à la liste
                concerts_temp.add(concert_temp);
            }
            
            MockData.setArtistes(artistes_temp);
            MockData.setScenes(scenes_temp);
            MockData.setConcerts(concerts_temp);

        } catch (IOException | ParserConfigurationException | SAXException ex) {
            System.err.println(ex);
        } catch (ParseException ex) {
            Logger.getLogger(FestineuchAppServletContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }

	System.out.println("ServletContextListener started");	
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
	System.out.println("ServletContextListener destroyed");
    }
}
