/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business;

import java.util.Date;

/**
 *
 * @author francis.heche
 */
public class Evenement {
    String id;
    String heure;
    String remarque;

    public Evenement(String id, String remarque){
        this.id = id;
        this.remarque = remarque;
    }

    public Evenement() {
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    @Override
    public String toString() {
        return "Evenement{" + "heure=" + heure + ", remarque=" + remarque + '}';
    }
    
    
}
