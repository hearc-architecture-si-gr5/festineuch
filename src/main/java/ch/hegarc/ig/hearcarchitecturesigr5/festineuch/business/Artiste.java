/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business;

/**
 *
 * @author francis.heche
 */
public class Artiste {
    String id;
    String nom;
    String description;
    String genre;
    String origine;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    @Override
    public String toString() {
        return "Artiste{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", genre=" + genre + ", origine=" + origine + '}';
    }
    
    
}
