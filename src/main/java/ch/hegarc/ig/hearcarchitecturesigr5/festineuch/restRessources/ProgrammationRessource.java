/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hegarc.ig.hearcarchitecturesigr5.festineuch.restRessources;

import ch.hearcarchitecturesigr5.festineuch.persistence.MockData;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Concert;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Scene;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Artiste;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Evenement;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author francis.heche
 */

@RequestScoped
@Path("/programmation")

public class ProgrammationRessource{
 
  @GET
  @Path("/concerts")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Concert> getConcerts() {
      return MockData.getConcerts();
  }    
  
  @GET
  @Path("/concerts/{concertid}")
  @Produces(MediaType.APPLICATION_JSON)
  public Concert getConcert(@PathParam("concertid") String concertid){
      return MockData.getConcert(concertid);
  }
  
  @GET
  @Path("/scenes")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Scene> getScenes(){     
      return MockData.getScenes();
  }     
  
  @GET
  @Path("/scenes/{sceneid}")
  @Produces(MediaType.APPLICATION_JSON)
  public Scene getScene(@PathParam("sceneid") String scenceid){       
      return MockData.getScene(scenceid);
  }
  
  @GET
  @Path("/artistes")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Artiste> getArtistes(){
      return MockData.getArtistes();
  }     
  
  @GET
  @Path("/artistes/{artisteid}")
  @Produces(MediaType.APPLICATION_JSON)
  public Artiste getArtiste(@PathParam("artisteid") String artisteid){     
      return MockData.getArtiste(artisteid);
  }
  
  @GET
  @Path("/evenements")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Evenement> getEvenements(){
      return MockData.getEvenements();
  }
  
  @GET
  @Path("/evenements/{evenementid}")
  @Produces(MediaType.APPLICATION_JSON)
  public Evenement getEvenement(@PathParam("evenementid") String evenementid){
      return MockData.getEvenement(evenementid);
  }
  
  @GET
  @Path("/concerts/{concertid}/evenements")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Evenement> getEvenementsDeConcert(@PathParam("concertid") String concertid){         
      return MockData.getConcert(concertid).getRemarques();
  }
 
  @POST
  @Path("/evenements")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.APPLICATION_JSON)
  public Evenement ajoutRemarque(
        @FormParam("concertid") String concertid,
        @FormParam("texte") String remarque
    ) throws Exception {
      Evenement evenement = new Evenement();
      try {
        evenement = MockData.ajoutRemarque(concertid, remarque);
      } catch (Exception e) {
        throw new Exception("400 - Un problème est survenu lors de l'ajout de l'événement");
      }
      return evenement;
  }
}