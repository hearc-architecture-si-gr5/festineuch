/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author francis.heche
 */
public class Concert {
    String id;
    Date date;
    Date heure_debut;
    Date heure_fin;
    List<Evenement> remarques = new ArrayList<>();
    Artiste artiste;
    Scene scene;

    public Concert() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getHeure_debut() {
        return heure_debut;
    }

    public void setHeure_debut(Date heure_debut) {
        this.heure_debut = heure_debut;
    }

    public Date getHeure_fin() {
        return heure_fin;
    }

    public void setHeure_fin(Date heure_fin) {
        this.heure_fin = heure_fin;
    }

    public List<Evenement> getRemarques() {
        return remarques;
    }

    public void setRemarques(List<Evenement> remarques) {
        this.remarques = remarques;
    }
    
    public void addRemarque(Evenement remarque) {
        this.remarques.add(remarque);
    }    

    public Artiste getArtiste() {
        return artiste;
    }

    public void setArtiste(Artiste artiste) {
        this.artiste = artiste;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @Override
    public String toString() {
        return "Concert{" + "id=" + id + ", date=" + date + ", heure_debut=" + heure_debut + ", heure_fin=" + heure_fin + ", remarques=" + remarques + ", artiste=" + artiste + ", scene=" + scene + '}';
    }
    
    
}
