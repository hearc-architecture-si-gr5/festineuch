package ch.hearcarchitecturesigr5.festineuch.persistence;

import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Concert;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Scene;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Artiste;
import ch.hegarc.ig.hearcarchitecturesigr5.festineuch.business.Evenement;
import java.util.ArrayList;
import java.util.List;

public class MockData {
    private static List<Concert> listeConcerts = new ArrayList();
    private static List<Scene> listeScenes = new ArrayList();
    private static List<Artiste> listeArtists = new ArrayList();
    private static List<Evenement> listeEvenements = new ArrayList();    

    public static void setConcerts(List<Concert> concerts) {
        listeConcerts = concerts;
    }    
    
    public static List<Concert> getConcerts() {
        return listeConcerts;
    }

    public static Concert getConcert(String customerid) {
        for (Concert c : listeConcerts) {
           if(c.getId().equals(customerid)){
               return c;
           }
        }
        return null;
    }

    public static void setScenes(List<Scene> scenes) {
        listeScenes = scenes;
    }    
    
    public static List<Scene> getScenes() {
        return listeScenes;
    }

    public static Scene getScene(String scenceid) {
        for (Scene s : listeScenes) {
           if(s.getId().equals(scenceid)){
               return s;
           }
        }
        return null;
    }
    
    public static void setArtistes(List<Artiste> artistes) {
        listeArtists=artistes;
    }    

    public static List<Artiste> getArtistes() {
       return listeArtists;
    }

    public static Artiste getArtiste(String artisteid) {
        for (Artiste a : listeArtists) {
           if(a.getId().equals(artisteid)){
               return a;
           }
        }
        return null;
    } 
    
    public static void setEvenements(List<Evenement> evenements) {
        listeEvenements=evenements;
    }    

    public static List<Evenement> getEvenements() {
       return listeEvenements;
    }

    public static Evenement getEvenement(String evenementid) {
        for (Evenement e : listeEvenements) {
           if(e.getId().equals(evenementid)){
               return e;
           }
        }
        return null;
    }

    public static Evenement ajoutRemarque(String concertid, String remarque) {
        Evenement e = new Evenement("410",remarque);
        
        for(Concert c : listeConcerts){
            if(c.getId().equals(concertid)){
               c.addRemarque(e);
            }
        }

        listeEvenements.add(e);
        return e;
    }
}
